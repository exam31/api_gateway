package v1

import (
	"context"
	"exam/api/models"
	"exam/genproto/review"
	l "exam/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create review...
// @Summary 		Create Review
// @Description 	review service create
// @Tags 			Review
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			body 	body 		review.ReviewRequest true "Review"
// @Success 		200 	{object} 	review.ReviewResponse
// @Failure 		400 	"Errorresponse"
// @Router 			/v1/review [post]
func (h *handlerV1) CreateReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var (
		body        review.ReviewRequest
		jsrsMarshal protojson.MarshalOptions
	)
	jsrsMarshal.UseEnumNumbers = true
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().CreateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create reveiw", l.Error(err))
	}
	c.JSON(http.StatusCreated, response)
}

// get review
// @Summary 		Get Review
// @Description 	this will display the review information
// @Tags 			Review
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			id 		path 		string true "id"
// @Success 		200 	{object} 	review.Reviews
// @Failure 		400 	"Errorresponse"
// @Router 			/v1/review/{id} [get]
func (h *handlerV1) GetReviewById(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var (
		body        review.ReviewId
		jsrsMarshal protojson.MarshalOptions
	)
	jsrsMarshal.UseEnumNumbers = true
	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	respose, err := h.serviceManager.ReviewService().GetReviewById(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error id of review")
	}
	c.JSON(http.StatusOK, respose)
}

// update review
// @Summary 		Update Review
// @Description 	this updating information of review
// @Tags 			Review
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			body 	body 	 review.ReviewUp true "Review"
// @Success 		200 	{object} review.ReviewResponse
// @Failure 		400 	"Errorresponse"
// @Failure 		500 	"Errorresponse"
// @Router 			/v1/review/update [put]
func (h *handlerV1) UpdateReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var (
		body        review.ReviewUp
		jsrsMarshal protojson.MarshalOptions
	)
	jsrsMarshal.UseEnumNumbers = true
	err = c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.ReviewService().UpdateReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error update review", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// delete review
// @Summary 		Delete Review
// @Description 	this deleting information of review
// @Tags 			Review
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			id 	path 		string true "id"
// @Success 		200 {object} 	review.Empty
// @Failure 		400 "ErrorResponse"
// @Router 			/v1/review/delete/{id} [delete]
func (h *handlerV1) DeleteReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var jsrsMarshal protojson.MarshalOptions
	jsrsMarshal.UseEnumNumbers = true

	var body review.ReviewId
	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ReviewService().DeleteReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while deleting review")
	}
	c.JSON(http.StatusOK, response)

}
