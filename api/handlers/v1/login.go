package v1

import (
	"context"
	"exam/api/models"
	"exam/genproto/customer"
	"exam/pkg/etc"
	"exam/pkg/logger"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// customer login
// @Summary 		Login Customer
// @Description 	This function get login customer
// @Tags 			Customer Register
// @Accept 			json
// @Produce			json
// @Param 			email 		path string true "email"
// @Param 			password 	path string true "password"
// @Success 		200 {object} 	customer.LoginResponse
// @Failure			500 {object} 	models.Error
// @Failure			400 {object} 	models.Error
// @Router			/v1/login/{email}/{password} [get]
func (h *handlerV1) Login(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseEnumNumbers = true
	var (
		password = c.Param("password")
		email    = c.Param("email")
	)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().GetByEmail(ctx, &customer.LoginRequest{
		Email: email,
	})

	if err != nil {
		c.JSON(http.StatusNotFound, models.Error{
			Error:       err,
			Description: "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting customer by email", logger.Any("post", err))
		return
	}

	fmt.Println(password, email)
	fmt.Println(res.Password)
	if !etc.CheckPasswordHash(password, res.Password) {
		fmt.Println(password)
		c.JSON(http.StatusNotFound, models.Error{
			Description: "Password or Email error",
			Code:        http.StatusBadRequest,
		})
		return
	}

	h.jwthandler.Iss = "user"
	h.jwthandler.Sub = res.Id
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log
	tokens, err := h.jwthandler.GenerateAuthJWT()
	refreshToken := tokens[1]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}

	res.RefreshToken = refreshToken
	res.Password = ""

	c.JSON(http.StatusOK, res)
}
