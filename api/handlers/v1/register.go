package v1

import (
	"context"
	"encoding/json"
	"exam/api/models"
	"exam/email"
	"exam/genproto/customer"
	"exam/pkg/etc"
	"exam/pkg/logger"
	"fmt"
	"net/http"
	"strings"
	"time"

	"google.golang.org/protobuf/encoding/protojson"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
)

// register customer
// @Summary		register customer
// @Description	this registers customer
// @Tags		Customer Register
// @Accept		json
// @Produce 	json
// @Param 		body	body  	 models.CustomerRegister true "Register customer"
// @Success		201 	{object} models.Error
// @Failure		500 	{object} models.Error
// @Router		/v1/customer/register 	[post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var (
		body        models.CustomerRegister
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	err := c.ShouldBindJSON(&body)

	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	emailExists, err := h.serviceManager.CustomerService().CheckField(ctx, &customer.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       err,
			Code:        http.StatusInternalServerError,
			Description: "Please check your email if it is true!",
		})
		h.log.Error("Error while cheking email uniqueness", logger.Any("check", err))
		return
	}

	if emailExists.Exists {
		c.JSON(http.StatusConflict, models.Error{
			Error: err,
		})
		return
	}

	uniqueEmailRedis, err := h.redis.Exists("user " + body.Email)

	str := cast.ToString(uniqueEmailRedis)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Something went wrong",
		})
		h.log.Error("Error while Checking company email uniqueness from redis", logger.Error(err))
		return
	}

	if str == "1" {
		c.JSON(http.StatusConflict, models.Error{
			Description: "Sorry Enter another company email",
		})
		return
	}
	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       err,
			Code:        http.StatusInternalServerError,
			Description: "Erter right info",
		})
		h.log.Error("couldn't hash the password")
		return
	}

	customerToSaved := &customer.CustomerRequest{
		FirstName:   body.FirstName,
		LastName:    body.LastName,
		Bio:         body.Bio,
		Email:       body.Email,
		Password:    body.Password,
		PhoneNumber: body.PhoneNumber,
	}
	for _, address := range body.Addresses {
		customerToSaved.Addresses = append(customerToSaved.Addresses, &customer.AddressReq{
			Country: address.Country,
			Street:  address.Street,
		})
	}

	customerToSaved.Code = etc.GenerateCode(6)

	msg := "Subject: Customer email verification\n Your verification code: " + customerToSaved.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}

	bodyByte, err := json.Marshal(customerToSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       err,
			Code:        http.StatusInternalServerError,
			Description: "Something went wrong",
		})
		h.log.Error("Error while marshlaing user to json", logger.Error(err))
		return
	}
	fmt.Println(customerToSaved.Email)
	err = h.redis.SetWithTTL(customerToSaved.Email, string(bodyByte), 600)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       err,
			Code:        http.StatusInternalServerError,
			Description: "Something went wrong",
		})
		h.log.Error("Error while saving json to redis", logger.Error(err))
		return
	}

	c.JSON(http.StatusAccepted, models.Error{
		Error:       nil,
		Code:        http.StatusAccepted,
		Description: "Your request successfuly accepted we have send code to your email, Your code is : " + customerToSaved.Code,
	})
}
