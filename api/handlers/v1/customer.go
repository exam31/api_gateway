package v1

import (
	"context"
	"exam/api/models"
	"exam/genproto/customer"
	l "exam/pkg/logger"
	"exam/pkg/utils"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// create 		customer
// @Summary 	this function creates the customers
// @Description this function includes all customers
// @Tags 		Customer
// @Security	BearerAuth
// @Accept 		json
// @Produce 	json
// @Param 		body 	body 		customer.CustomerRequest true "Customer"
// @Success	 	200 	{object} 	customer.CustomerResponse
// @Failure 	400 	{object}	models.Error
// @Router 		/v1/customer [post]
func (h *handlerV1) Create(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}

	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		body        customer.CustomerRequest
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseProtoNames = true
	err = c.ShouldBindJSON(&body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.CustomerService().Create(ctx, &body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create customer", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// get customer
// @Summary 	this function getting the customers posts
// @Description this function select the customers posts
// @Tags 		Customer
// @Security    BearerAuth
// @Accept 		json
// @Produce 	json
// @Param id 	path 	string	 	true 	"uuid"
// @Success 	200 	{object} 	customer.CustomerInfo
// @Failure 	400 	{object}	models.Error
// @Router 		/v1/customer/post/review/{id} [get]
func (h *handlerV1) GetCustomerInfo(c *gin.Context) {
	claims, err := GetClaims(*h, c)

	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}

	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseEnumNumbers = true

	var body customer.CustomerID

	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responseCus, err := h.serviceManager.CustomerService().GetCustomerInfo(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get customer", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, responseCus)
}

// Get Customer with search
// @Summary 	Customer list getting
// @Description Customer list getting
// @Tags		Customer
// @Security	BearerAuth
// @Accept		json
// @Produce		json
// @Param		page 		query string false "page"
// @Param 		limit		query string false "limit"
// @Param 		order		query string false "Order format should be 'key:value'"
// @Param		search		query string false 	"Search format should be 'key:value'"
// @Success		200		{object}	customer.CustomerAll
// @Failure		400 	{object}	models.Error
// @Failure		500 	{object}	models.Error
// @Router		/v1/customer/search	[get]
func (h *handlerV1) GetCustomerBySearchOrder(c *gin.Context) {

	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseEnumNumbers = true

	queryParams := c.Request.URL.Query()

	search := strings.Split(c.Query("search"), ":")
	order := strings.Split(c.Query("order"), ":")

	if len(search) != 2 && len(order) != 2 {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Description: "Enter needed params",
		})
		h.log.Error("failed to get all params")
		return
	}
	params, errStr := utils.ParseQueryParams(queryParams)

	if errStr != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errStr[0],
		})
		h.log.Error("failed to parse query params json " + errStr[0])
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, errs := h.serviceManager.CustomerService().GetCustomerBySearchOrder(ctx, &customer.GetListUserRequest{
		Page:   params.Page,
		Limit:  params.Limit,
		Search: &customer.Search{Field: search[0], Value: search[1]},
		Orders: &customer.Order{Field: order[0], Value: order[1]},
	})
	if errs != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Description: "couldn't Get",
		})
		h.log.Error("Get Customers with search and order", l.Error(errs))
		return
	}
	c.JSON(http.StatusOK, response)
}

// user update
// @Summary 		this function update
// @Description 	this function updating the customers
// @Tags 			Customer
// @Security        BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			customerbody body 			customer.CustomerUp true "Update Customer"
// @Success 		200 		{object} 		customer.CustomerResponse
// @Failure 		400 		{object}		models.Error
// @Router 			/v1/customer/update [put]
func (h *handlerV1) UpdateCustomer(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		customerbody customer.CustomerUp
		jspbMarshal  protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	err = c.ShouldBindJSON(&customerbody)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.CustomerService().UpdateCustomer(ctx, &customer.CustomerUp{
		Id:          customerbody.Id,
		FirstName:   customerbody.FirstName,
		LastName:    customerbody.LastName,
		Bio:         customerbody.Bio,
		Addresses:   customerbody.Addresses,
		Email:       customerbody.Email,
		PhoneNumber: customerbody.PhoneNumber,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}
	c.JSON(http.StatusCreated, response)
}

// delete 		customer
// @Summary 	this function delete
// @Description this function delting customer
// @Tags 		Customer
// @Security    BearerAuth
// @Accept 		json
// @Produce 	json
// @Param 		id 	path 		string	 true "delete Customer"
// @Success 	200 {object}  	customer.CustomerResponse
// @Failure 	400 {object}	models.Error
// @Router 		/v1/customer/{id} [delete]
func (h *handlerV1) DeleteCustomer(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		body        customer.CustomerID
		jspbMarshal protojson.MarshalOptions
	)

	jspbMarshal.UseEnumNumbers = true

	body.Id = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	customerResponse, err := h.serviceManager.CustomerService().DeleteCustomer(ctx, &body)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update user", l.Error(err))
	}

	c.JSON(http.StatusCreated, customerResponse)

}
