package v1

import (
	"context"
	"exam/api/models"
	"exam/genproto/post"
	l "exam/pkg/logger"
	"exam/pkg/utils"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create 		post...
// @Summary	 	Create 	Post
// @Description post 	service create
// @Tags 		Post
// @Security	BearerAuth
// @Accept 		json
// @Produce 	json
// @Param 		post 	body  	 models.Post true "Post"
// @Success 	200 	{object} post.PostResp
// @Failure 	400 	"Errorresponse"
// @Router 		/v1/posts [post]
func (h *handlerV1) CreatePost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		post         post.PostReq
		jspbpMarshal protojson.MarshalOptions
	)

	jspbpMarshal.UseProtoNames = true

	err = c.ShouldBindJSON(&post)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	postResponse, err := h.serviceManager.PostService().Create(ctx, &post)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create post", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, postResponse)
}

// get 			post 	review
// @Summary 	Get 	Post Review
// @Description this 	will display the post review information
// @Tags 		Post
// @Security	BearerAuth
// @Accept 		json
// @Produce 	json
// @Param 		id 		path 	 string true "id"
// @Success 	200 	{object} post.PostInfo
// @Failure 	400 	"Errorresponse"
// @Router /v1/posts/review/get/{id} [get]
func (h *handlerV1) GetPostReview(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		jspbpMarshal protojson.MarshalOptions
		body         post.ID
	)
	jspbpMarshal.UseEnumNumbers = true
	body.PostID = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	responsePost, err := h.serviceManager.PostService().GetPostReview(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get post review", l.Error(err))
		return
	}
	c.JSON(http.StatusOK, responsePost)
}

// list post
// @Summary 		list 	post
// @Description 	lists 	posts
// @Tags 			Post
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			page 	query 	string false "page"
// @Param 			limit 	query 	string false "limit"
// @Param			search	query 	string false "search"
// @Success 		200 	{object} post.ListPostResponse
// @Failure 		400 	{object} models.Error
// @Router /v1/posts/list 	[get]
func (h *handlerV1) ListPost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var jspbpMarshal protojson.MarshalOptions
	jspbpMarshal.UseEnumNumbers = true
	queryParams := c.Request.URL.Query()

	params, errStr := utils.ParseQueryParams(queryParams)

	if errStr != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": errStr[0],
		})
		h.log.Error("failed to parse query params json " + errStr[0])
		return
	}
	fmt.Println(params)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, errs := h.serviceManager.PostService().ListPost(ctx, &post.ListPostRequest{
		Page:  params.Page,
		Limit: params.Limit,
		Value: params.Search,
	})
	if errStr != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": errs.Error(),
		})
		h.log.Error("failed to get post review", l.Error(errs))
		return
	}

	c.JSON(http.StatusOK, response)
}

// update 			post
// @Summary 		Update 		Post
// @Description 	this 		updating information of post
// @Tags 			Post
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			postbody 	body post.PostUp true "Post"
// @Success 		200 		{object} post.PostReq
// @Failure 		400 		"Errorresponse"
// @Failure 		500 		"Errorresponse"
// @Router 			/v1/posts/update [put]
func (h *handlerV1) UpdatePost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}
	var (
		postbody     post.PostUp
		jspbpMarshal protojson.MarshalOptions
	)
	jspbpMarshal.UseEnumNumbers = true
	err = c.ShouldBindJSON(&postbody)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update post", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	postResponse, err := h.serviceManager.PostService().UpdatePost(ctx, &post.PostUp{
		Id:          postbody.Id,
		OwnerId:     postbody.OwnerId,
		Name:        postbody.Name,
		Description: postbody.Description,
		Medias:      postbody.Medias,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to update post service", l.Error(err))
		return
	}
	c.JSON(http.StatusCreated, postResponse)
}

// delete 			post
// @Summary 		Delete 	Post
// @Description 	this 	deleting information of post
// @Tags 			Post
// @Security		BearerAuth
// @Accept 			json
// @Produce 		json
// @Param 			id 		path string true "id"
// @Success 		200 	{object} post.Empty
// @Failure 		400 	"Errorresponse"
// @Router 			/v1/posts/review/delete/{id} [delete]
func (h *handlerV1) DeletePost(c *gin.Context) {
	claims, err := GetClaims(*h, c)
	if err != nil {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Error:       err,
			Description: "You are not authorized",
		})
		h.log.Error("Checking Authorization", l.Error(err))
		return
	}
	if claims.Role != "authorized" {
		c.JSON(http.StatusUnauthorized, models.Error{
			Code:        http.StatusUnauthorized,
			Description: "You are not authorized",
		})
		return
	}

	var (
		body         post.ID
		jspbpMarshal protojson.MarshalOptions
	)
	jspbpMarshal.UseEnumNumbers = true

	body.PostID = c.Query("id")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.PostService().DeletePost(ctx, &body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to delete post service")
	}
	c.JSON(http.StatusOK, response)
}
