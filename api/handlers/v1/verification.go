package v1

import (
	"context"
	"encoding/json"
	"exam/api/models"
	"exam/genproto/customer"
	l "exam/pkg/logger"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Customer Register
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  models.VerifyResponse
// @Router      /v1/verify/{email}/{code} [get]
func (h *handlerV1) Verification(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	var (
		code  = c.Param("code")
		email = c.Param("email")
	)
	sredis, err := h.redis.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting user from redis", l.Any("redis", err))
		return
	}

	cs := cast.ToString(sredis)

	csStr := models.Customer{}

	err = json.Unmarshal([]byte(cs), &csStr)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to user csStr", l.Any("json", err))
		return
	}

	if csStr.Code != code {
		fmt.Println(csStr.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}

	id := uuid.New()
	body := customer.CustomerRequest{
		FirstName:    csStr.FirstName,
		LastName:     csStr.LastName,
		Bio:          csStr.Bio,
		Email:        csStr.Email,
		Password:     csStr.Password,
		RefreshToken: csStr.RefreshToken,
		Uuid:         id.String(),
	}

	h.jwthandler.Iss = "customer"
	h.jwthandler.Sub = body.Uuid
	h.jwthandler.Role = "authorized"
	h.jwthandler.Aud = []string{"exam-app"}
	h.jwthandler.SigninKey = h.cfg.SignInKey
	h.jwthandler.Log = h.log

	tokens, err := h.jwthandler.GenerateAuthJWT()
	accessToken := tokens[0]
	refreshToken := tokens[1]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	body.RefreshToken = refreshToken

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.CustomerService().Create(ctx, &body)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer", l.Any("post", err))
		return
	}

	response := models.VerifyResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Bio:         res.Bio,
		Email:       res.Email,
		PhoneNumber: res.PhoneNumber,
	}

	response.AccessToken = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)
}
