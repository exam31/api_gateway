package models

type Post struct {
	Owner_id    string
	Name        string
	Description string
	Medias      []Media
}
type Media struct {
	Name string
	Link string
	Type string
}
type CustomerRegister struct {
	FirstName   string    `json:"first_name"`
	LastName    string    `json:"last_name"`
	Bio         string    `json:"bio"`
	Email       string    `json:"email"`
	Password    string    `json:"password"`
	PhoneNumber string    `json:"phone_number"`
	Addresses   []Address `json:"addresses"`
}
type Customer struct {
	FirstName    string
	LastName     string
	Bio          string
	Email        string
	Password     string
	PhoneNumber  string
	Code         string
	RefreshToken string
}
type Address struct {
	Country string
	Street  string
}

type Error struct {
	Code        int
	Error       error
	Description string
}
type VerifyResponse struct {
	Id           string
	FirstName    string
	LastName     string
	Bio          string
	Email        string
	PhoneNumber  string
	AccessToken  string
	RefreshToken string
}

type AddressResponse struct {
	Id      string
	OwnerId string
	Country string
	Street  string
}
